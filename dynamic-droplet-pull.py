#!/usr/bin/env python3
import os
import sys
import json
from pydo import Client

def get_1password_token():
    try:
        # Run the 1Password CLI command to retrieve the DigitalOcean API token
        print("Fetching DigitalOcean API token from 1Password...")
        token = os.popen('op read "op://Private/DigitalOcean API Token/token"').read().strip()
        if token:
            print("DigitalOcean API token retrieved successfully.")
            return token
        else:
            print("Error: Failed to retrieve DigitalOcean API token from 1Password.")
            return None
    except Exception as e:
        print("Error:", e)
        return None

# Get the DigitalOcean API token from 1Password
print("Getting DigitalOcean API token...")
DO_API_TOKEN = get_1password_token()

if not DO_API_TOKEN:
    print("Please set your DigitalOcean API token in 1Password under the item 'DigitalOcean API Token'.")
    sys.exit(1)

#print("DigitalOcean API token:", DO_API_TOKEN)

# Initialize DigitalOcean manager
print("Initializing DigitalOcean manager...")
do_manager = Client(DO_API_TOKEN)

# Fetch all droplets
print("Fetching all active droplets from DigitalOcean...")
droplets_response = do_manager.droplets.list()

# Extract droplets from the response
droplets = droplets_response.get('droplets', [])

# Check if droplets is a list
if not isinstance(droplets, list):
    print("Error: Failed to fetch droplets from DigitalOcean.")
    sys.exit(1)

# Create inventory dictionary
print("Creating inventory dictionary...")
inventory = {
    '_meta': {
        'hostvars': {}
    },
    'droplets': {
        'hosts': [],
        'vars': {}
    }
}

# Populate inventory with droplets
print("Populating inventory with droplets...")
for droplet in droplets:
    print(f"Adding droplet {droplet.get('name', 'Unknown')} to inventory...")
    # Get the public IP address
    public_ip = next((network['ip_address'] for network in droplet.get('networks', {}).get('v4', []) if network.get('type') == 'public'), None)
    if public_ip:
        inventory['_meta']['hostvars'][droplet['name']] = {
            'ansible_host': public_ip
            # Add more hostvars as needed
        }
        inventory['droplets']['hosts'].append(droplet['name'])
    else:
        print(f"No public IP address found for droplet {droplet.get('name', 'Unknown')}")

# Define the path to the Ansible inventory file
inventory_file_path = 'inventory.json'

# Write the inventory dictionary to the inventory file
with open(inventory_file_path, 'w') as inventory_file:
    json.dump(inventory, inventory_file)

print(f"Inventory file saved to {inventory_file_path}")
